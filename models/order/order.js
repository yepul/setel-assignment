const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let orderSchema = new Schema ({
    order : {type: String},
    state : {type: String},
    price : {type: Number},
    createdAt : {type: Date, default: Date.now},
    confirmedAt : {type: Date},
    cancelledAt : {type: Date},
    deliveredAt : {type: Date},
    details : {type: String},
    extraInfo : {type: String},
	accessToken : {type: String},
})

module.exports = mongoose.model('Order', orderSchema);