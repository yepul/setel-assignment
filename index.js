const express = require('express');
const bodyParser = require('body-parser');
var mongoose = require('mongoose'); 
const fs = require('fs');

const app = express(); 
const api  = require('./routes/api/index');
const mongoDB = 'mongodb://localhost/setel_assignment';
mongoose.connect(mongoDB, { useNewUrlParser: true });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const port = process.env.PORT || 8000; 

app.use('/api', api);
app.listen(port);
console.log("Connected to port : " + port);
