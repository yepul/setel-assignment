let express = require('express');
let router = express.Router();
let multiparty = require('connect-multiparty');
let moment = require('moment');
let async = require('async');

let Order = require('../../models/order/order');
let Payment = require('../../models/payment/payment');

var multipartyMiddleware = multiparty();
router.use(multipartyMiddleware);

router.post('/create-order', (req,res)=>{
    let order = req.body.order
    let price = parseInt(req.body.price * 100) / 100
    let details = req.body.details
    let extraInfo = (req.body.extraInfo ? req.body.extraInfo : "")
    let accessToken = req.body.accessToken

    if(!order || order == ""){
        res.json({status : 400, message : "Error! Invalid order."})
    } else if (!price || price <= 0){
        res.json({status : 401, message : "Error! Invalid price."})
    } else if(!details || details == ""){
        res.json({status : 402, message : "Error! Invalid details."})
    } else if(!accessToken || accessToken == "" /* || !validateAccessToken(accessToken) */){ /* can do validation here */ 
        res.json({status : 403, message : "Error! Invalid Access Token."})
    } else{
        let newOrder = new Order({
            order : order,
            price : price,
            state : "created",
            details : details,
            accessToken : accessToken,
            extraInfo : extraInfo,
        })
        newOrder.save((err, createdOrder) =>{
            if (createdOrder){
                // console.log(createdOrder._id)
                res.json({status : 200, message : "Order Created.", orderId : createdOrder._id})
            } else {
                res.json({status : 404, message : "Unknown error occurred!"})
            }
        })
    }
})

router.get('/get-order-all', (req,res)=>{  
    Order.find({}).exec((err,allOrder)=>{
        if(allOrder && allOrder.length > 0){
            res.json({status : 200, data : allOrder})
        } else{
            res.json({status : 400, message : "No data found."})
        }
    })  
});

router.get('/get-order-details/:orderId', (req,res)=>{
    let orderId = req.params.orderId
    if(orderId && orderId != ""){
        Order.findById(orderId).exec((err,orderDetails)=>{
            // console.log(orderDetails)
            if(orderDetails){
                res.json({
                    status : 200, 
                    orderId : orderDetails._id, 
                    order : orderDetails.order, 
                    price : orderDetails.price, 
                    state : orderDetails.state, 
                    details : orderDetails.details,
                    extraInfo : orderDetails.extraInfo,
                })        
            }else{
                res.json({status : 400, message : "Invalid Order ID"})
            }
        })
    }else{
        res.json({status : 400, message : "Invalid Order ID"})
    }
}) 

router.post('/confirm-order',(req,res)=>{
    let orderId = req.body.orderId
    Order.findById(orderId).exec((err, orderDetails)=>{
        if(orderDetails){
            if(orderDetails.state == "created"){
                Order.findByIdAndUpdate(orderId,{state : "confirmed", confirmedAt : moment()}).exec((err,done)=>{
                    // console.log(done)
                    if(done){
                        let newPayment = new Payment({
                            orderId : orderId,
                            status : "created",
                            price : orderDetails.price,
                        })
                        newPayment.save((err, createdPayment) =>{
                            if(createdPayment){
                                res.json({status : 200, message : "Order Confirm."})
                            }else{
                                res.json({status : 400, message : "Error! Failed to confirm order."})
                            }
                        })
                    }else{
                        res.json({status : 400, message : "Error! Failed to confirm order."})
                    }
                })
            }else{
                res.json({status : 400, message : "Error! Failed to confirm order."})
            }
        }else{
            res.json({status : 401, message : "Error! Order not found."})
        }
    })
})

router.post('/cancel-order',(req,res)=>{
    let orderId = req.body.orderId
    Order.findById(orderId).exec((err, orderDetails)=>{
        if(orderDetails){
            if(orderDetails.state != "cancelled"){
                Order.findByIdAndUpdate(orderId,{state : "cancelled", cancelledAt : moment()}).exec((err,done)=>{
                    if(done){
                        res.json({status : 200, message : "Order cancelled."})
                    }else{
                        res.json({status : 400, message : "Error! Failed to cancel order."})
                    }
                })
            }else{
                res.json({status : 400, message : "Error! Failed to cancel order."})
            }
        }else{
            res.json({status : 401, message : "Error! Order not found."})
        }
    })
})

router.post('/request-payment',(req,res)=>{
    let orderId = req.body.orderId
    let paymentVerification = Math.floor(100000 + Math.random() * 900000)

    Payment.update({orderId : orderId},{status : "confirmed", paymentVerification : paymentVerification}).exec((err,requestPayment)=>{
        if(requestPayment){
            res.json({status : 200, paymentVerification : paymentVerification, message : "Confirm payment ? (confirmed, declined)"})
        }else{
            res.json({status : 400, message : "Error! Invalid payment."})
        }
    })

})

router.post('/confirm-payment',(req,res)=>{
    let orderId = req.body.orderId
    let paymentVerification = req.body.paymentVerification
    let confirmation = req.body.confirmation /* (confirmed or declined) */
    let amountPaid = parseInt(req.body.amountPaid * 100) / 100

    if(confirmation && confirmation == "confirmed"){
        Payment.find({orderId : orderId, status : "confirmed", paymentVerification : paymentVerification}).exec((err,verifyPayment)=>{
            if(verifyPayment && verifyPayment.length > 0){
                // console.log(verifyPayment);
                Order.find({_id : orderId, state : "confirmed"}).exec((err,orderDetails)=>{
                    // console.log(orderDetails);
                    if(orderDetails && orderDetails.length > 0){
                        if(amountPaid && amountPaid >= orderDetails[0].price){
                            let amountBalance = parseInt((amountPaid - orderDetails[0].price) * 100) / 100
                            Payment.update(
                                {orderId : orderId, status : "confirmed"},
                                {
                                    amountPaid : amountPaid, 
                                    amountBalance : amountBalance, 
                                    status : "paid",
                                    paidAt : moment(),
                                }
                            ).exec((err,confirmPayment)=>{
                                if(confirmPayment){
                                    Order.findByIdAndUpdate(orderId,{state : "delivered", deliveredAt : moment()}).exec((err,done)=>{
                                        if(done){
                                            res.json({status : 200, amountBalance : amountBalance, message : "Payment complete. The item will be delivered shortly."})
                                        }else{
                                            res.json({status : 400, message : "Error! Invalid payment."})
                                        }
                                    })
                                }else{
                                    res.json({status : 400, message : "Error! Invalid payment."})
                                }
                            })
                        }else{
                            res.json({status : 401, message : "Error! Insufficient Funds."})
                        }
                    }else{
                        res.json({status : 402, message : "Error! Payment confirmation failed."})
                    }
                })
            }else{
                res.json({status : 403, message : "Error! Invalid payment verification."})
            }
        })
    }else if(confirmation && confirmation == "declined"){
        Order.findByIdAndUpdate(orderId,{state : "cancelled", cancelledAt : moment()}).exec((err,done)=>{
            if(done){
                res.json({status : 300, message : "Order cancelled."})
            }else{
                res.json({status : 404, message : "Error! Failed to cancel order."})
            }
        })
    }else{
        res.json({status : 400, message : "Error! Invalid payment."})
    }
})

module.exports = router;