const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let paymentSchema = new Schema ({
    orderId: {type: Schema.Types.ObjectId, ref: 'Order'},
    createdAt : {type: Date, default: Date.now},
    paidAt : {type: Date},
    status : {type: String},
    price : {type: Number},
    amountPaid : {type: Number},
    amountBalance : {type: Number},
    paymentVerification : {type: String},

})

module.exports = mongoose.model('Payment', paymentSchema);